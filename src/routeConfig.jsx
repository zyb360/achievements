// eslint-disable-next-line no-use-before-define
import React, { lazy } from 'react';
import { Route } from 'react-router-dom';

const Index = lazy(() => import('@pages/home/index'));

const ido = lazy(() => import('@pages/ido/index'));
const routes = [
    {
        // 首页
        path: '/',
        component: Index
    },
    {
        // 首页
        path: '/ido',
        component: ido
    }
    
    
    
];

const rootConfig = routes.map(route => {
    const { component: Component } = route;
    return <Route key={route.path} path={route.path} exact component={Component} />;
});
export default rootConfig;
