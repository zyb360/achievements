/* eslint-disable no-undef */
import Web3 from 'web3';
import Contracts from './contracts.json';
import BigNumber from 'bignumber.js';


const sendWeb3 = new Web3(window.ethereum);
const keys = Object.keys(Contracts),
    callContracts = {},
    sendContracts = {};

for (const key of keys) {
    sendContracts[key] = new sendWeb3.eth.Contract(Contracts[key].abi, Contracts[key].address);
}


export { callContracts, sendContracts, sendWeb3 };

// 精度转换
function toDecimals(decimals) {
    return '10'.toBN().pow(decimals);
}
function decode(types) {
    const _this = this.slice(0, 2) === '0x' ? `${this}` : `0x${this}`;

    if (types.length > 1) {
        return sendWeb3.eth.abi.decodeParameters(types, _this);
    }

    return sendWeb3.eth.abi.decodeParameter(types[0], _this);
}
function toBN() {
    const _this = `${this}`;

    if (Web3.utils.isAddress(_this)) {
        return new BigNumber(Web3.utils.toBN(_this).toString());
    }

    return new BigNumber(_this);
};

function toWei(decimals = 18) {
    return this.toBN().times(toDecimals(decimals)).toFixed();
}
function fromWei(decimals = 18) {
    const _this = `${this}`;
    if (Web3.utils.isHex(_this) && Number(this) === 0 || _this === '0x') {
        return '0';
    }

    return this.toBN().div(toDecimals(decimals)).toFixed();
}
function toFixed(decimal = 4) {
    const _this = typeof this === 'string' ? this : `${this}`;
    const index = _this.indexOf('.');

    if (index > 0) {
        if (_this.slice(index + 1).length <= decimal) {
            return _this;
        }

        if (decimal > 0) {
            return `${Number(`${_this.slice(0, index)}.${_this.slice(index + 1, index + 1 + decimal)}`)}`;
        }

        return `${Number(_this.slice(0, index))}`;
    }

    return _this;
}




// Defined string tool method
String.prototype.toBN = toBN;
String.prototype.toWei = toWei;
String.prototype.fromWei = fromWei;
String.prototype.toFixed = toFixed;
String.prototype.decode = decode;

// Defined number tool method
Number.prototype.toBN = toBN;
Number.prototype.toWei = toWei;
Number.prototype.fromWei = fromWei;
Number.prototype.toFixed = toFixed;

