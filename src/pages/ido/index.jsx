import { Component } from 'react';
import { Button, Select, Input } from 'antd';
const { Option } = Select;
import { sendWeb3, sendContracts } from '@utils/index';

import { messageWay } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            genesisState: '',
            idoPrice: 0,
            inputPrice: ''
        };
    }
    async componentDidMount() {
        await this.init();
    }
    init = async () => {

        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
        let result = await sendContracts.Genesis.methods.state().call();
       

        this.idoInfo();
    }
    changeState = async (e) => {


    }



    changeIdoPrice = () => {
        sendContracts.Genesis.methods.m_setPrice(this.state.inputPrice.toWei()).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('设置成功', 'success');
            this.init();
        }).on('error', err => {
            messageWay('取消', 'error');
        });
    }
    submit = () => {
        sendContracts.Genesis.methods.m_genesisMint().send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('设置成功', 'success');
            this.init();
        }).on('error', err => {
            messageWay('取消', 'error');
        });
    }
    render() {
        const state = this.state;
        return (
            <div className='ido'>
                <div className='box' >
                    <div className='title'>当前状态: {global.t(`ido.${state.genesisState}`)}</div>

                    
                </div>
                <div className='box' >
                    <div className='title'>当前参与创世铸造价格: {state.idoPrice}USDT</div>

                    <div className='item' id='language-change'>
                        <span>价格：</span>
                        <Input onInput={this.setIdoPrice} value={state.inputPrice}></Input>

                        <Button className='same-btn' onClick={this.changeIdoPrice}>设置</Button>
                    </div>
                </div>
                <div className='box' >
                    <div className='title'>创世铸造代币</div>

                    <div className='item' id='language-change'>
                        <Button className='same-btn' onClick={this.submit}>铸造</Button>
                    </div>
                </div>
            </div>
        );
    }
}
