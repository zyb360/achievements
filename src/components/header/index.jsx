// import { useState, useRef, useEffect } from 'react';
import { useState, useEffect } from 'react';
// import { Button, Menu, Modal, Select, message } from 'antd';
import { Button, Menu, message } from 'antd';
// import MenuItem from 'antd/lib/menu/MenuItem';
// import { withRouter, useLocation } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { sendWeb3 } from '@utils/index';
import logoImg from '@assets/logo.png';
import menuIcon from '@assets/images/menu.png';
import img from '@assets/images/language.png';

import './index.less';
import '../../styles/index.less';
const { SubMenu } = Menu;


function HaloHeader(props) {
    const { t } = useTranslation();
    // const [menu, useMenu] = useState('');

    const [address, useAddress] = useState();
    const [name, useName] = useState('/');

    const router = () => {
        let name = props.location.pathname;

        if (name == '/') {
            useName('/');
        } else {
            useName(name.split('/')[1]);
        }
    };

    useEffect(() => {
        router();
    }, [name, props.location.pathname]);

    useEffect(() => {
        (async () => {
            let account = await sendWeb3.eth.getAccounts();
            useAddress(account[0]);
        })();
    }, [sendWeb3]);

    const changeMenu = e => {
        message.destroy();
        console.log(e);



        if (
            e.key == '/'
        ) {
            if (!global.isPC) {
                let eleMenu = document.querySelectorAll('.mobile-menu');
                let eleDrawer = document.querySelectorAll('.drawer-mask');
                eleMenu[0].classList.add('show-menu');
                eleMenu[0].classList.remove('show-menu');
                eleDrawer[0].classList.remove('drawer-show');
            }


            props.history.push(e.key);
            // location.reload();
        }
    };

    const openMenu = e => {
        let eleMenu = document.querySelectorAll('.mobile-menu');
        let eleDrawer = document.querySelectorAll('.drawer-mask');
        eleMenu[0].classList.add('show-menu');
        if (e) {
            eleMenu[0].classList.add('show-menu');
            eleDrawer[0].classList.add('drawer-show');
        } else {
            eleMenu[0].classList.remove('show-menu');
            eleDrawer[0].classList.remove('drawer-show');
        }
    };


    const openModal = async () => {
        if (window.ethereum) {
            window.ethereum
                .request({ method: 'eth_requestAccounts' })
                .then(accounts => {
                    if (accounts[0]) {
                        useAddress(accounts[0]);
                    }
                    sessionStorage.setItem('walletType', 'Metamask');
                })
                .catch(err => {
                    console.error(err);
                });
        }
    };

    return props.isPC ? (
        <div className="main-header">
            {
                location.hash == '#/' ?
                    <header className=" ishome">
                        <div className="container">
                            {/* <div>
                        <img
                            className="logo-img"
                            src={logoImg}
                            onClick={changeMenu.bind(this, { key: '/' })}
                        ></img>
                    </div> */}
                            <Menu
                                mode="horizontal"
                                onClick={changeMenu}
                                className="hd-menu"
                                theme="dark"
                                defaultOpenKeys={[name]}
                                defaultSelectedKeys={[name]}
                                selectedKeys={[name]}
                            >

                            </Menu>

                            {/* <div className="hd_wallet">
                        <div>
                            {!address ? (
                                <div className="ConnectWallet">
                                    <Button onClick={openModal}>{t('ConnectWallet')}</Button>
                                </div>
                            ) : (
                                <div className="wallet_address">
                                    <div>
                                        {address
                                            ? `${address.slice(0, 5)} ... ${address.slice(-5)}`
                                            : ''}
                                    </div>
                                </div>
                            )}
                        </div>
                    </div> */}
                        </div>
                    </header> :
                    <header className="home-hd">
                        <div>
                            {/* <img
                    className="logo-img"
                    src={logoImg}
                    onClick={changeMenu.bind(this, { key: '/' })}
                ></img> */}
                        </div>
                        <Menu
                            mode="inline"
                            onClick={changeMenu}
                            className="hd-menu"
                            theme="dark"
                            defaultOpenKeys={[name]}
                            defaultSelectedKeys={[name]}
                            selectedKeys={[name]}
                        >
                            <SubMenu
                                className="bt"
                                title={
                                    <div>

                                        <span>创世铸造</span>
                                    </div>
                                }
                                key="applications"
                            >
                                <Menu.Item key="/">创世铸造</Menu.Item>
                            </SubMenu>


                        </Menu>
                    </header>

            }
        </div>
    ) : (
        <div className="mobile-header">
            <header className="mobile-hd">
                <div className="mobile-hd-view">
                    {/* <img src={logoImg} /> */}
                    <div className="hd_wallet">
                        <div>
                            {!address ? (
                                <div className="ConnectWallet">
                                    <Button onClick={openModal}>{t('ConnectWallet')}</Button>
                                </div>
                            ) : (
                                <div className="wallet_address">
                                    <div>
                                        {address
                                            ? `${address.slice(0, 5)} ... ${address.slice(-5)}`
                                            : ''}
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>

                    {/* <div className="">
                        <img
                            className="mobile-menu-img"
                            src={menuIcon}
                            onClick={openMenu.bind(this, true)}
                        ></img>
                    </div> */}
                </div>
            </header>


            <div className="mobile-menu">
                <div>
                    <Menu
                        mode="inline"
                        inlineCollapsed={false}
                        onClick={changeMenu}
                        className="hd-menu"
                        defaultOpenKeys={[name]}
                        defaultSelectedKeys={[name]}
                        selectedKeys={[name]}
                    >
                        <Menu.Item key="/" className="home-icon">
                            <div className="">
                                <span>{t('menu.home')}</span>
                                <img src={require('@assets/images/home.png')}></img>
                            </div>
                        </Menu.Item>

                    </Menu>
                    {/* <div className="line"></div>
                    <div className="language-title">
                        <span>{global.t('language')}</span>
                        <img src={img}></img>
                    </div> */}

                </div>
            </div>
            <div className="drawer-mask" onClick={openMenu.bind(this, false)}></div>
        </div>
    );
}

export default withRouter(HaloHeader);
