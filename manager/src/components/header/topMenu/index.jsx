import { useState, useRef, useEffect } from 'react';
import { withRouter, useLocation } from 'react-router-dom';
import { Button, message, Modal, Select } from 'antd';
import { getSessionStorage } from '../../../utils';
import '../../../styles/index.less';

function topMenu() {
    const [address, useAddress] = useState(getSessionStorage('address'));

    const { pathname } = useLocation();
    const [isurl, useIsurl] = useState(false);
    

    useEffect(() => {
        if (
            pathname == '/bridgePage' ||
            pathname == '/pos' ||
            pathname == '/posHistory' ||
            pathname == '/hashrateDetail'
        ) {
            useIsurl(true);
        } else {
            useIsurl(false);
        }
    }, [pathname]);

    const openModal = async () => {
        if (window.ethereum) {
            window.ethereum
                .request({ method: 'eth_requestAccounts' })
                .then(accounts => {
                    if (accounts[0]) {
                        useAddress(accounts[0]);
                    }
                    sessionStorage.setItem('walletType', 'Metamask');
                })
                .catch(err => {
                    console.error(err);
                });
        }
    };
    return (
        <div>
            {

                global.isPC && location.hash != '#/' ? (
                    <div className="oneTop" style={!global.$address ? { lineHeight: '30px' } : {}}>

                        {global.$address ? (
                            <div className="left">
                                <div>
                                    <div className="top">

                                        <div>
                                            {global.$address.slice(0, 5) + '...' + global.$address.slice(-5)}
                                        </div>
                                    </div>
                                    
                                </div>


                            </div>
                        ) : (

                            <div className="ConnectWallet">
                                <Button onClick={openModal}>{global.t('ConnectWallet')}</Button>
                            </div>

                        )}

                    </div>
                ) : (
                    ''
                )
                // )
            }

            
        </div>
    );
}
export default topMenu;
