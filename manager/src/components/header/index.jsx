import { useState, useRef, useEffect } from 'react';
import { Button, Menu, Modal, Select, message } from 'antd';
import MenuItem from 'antd/lib/menu/MenuItem';
import { withRouter, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import logoImg from '@assets/logo.png';

import menuIcon from '@assets/images/menu.png';
//memu
import { DownOutlined } from '@ant-design/icons';
import './index.less';
import '../../styles/index.less';
// import SubMenu from 'antd/lib/menu/SubMenu';
const { SubMenu } = Menu;

import { getSessionStorage, sendWeb3 } from '../../utils';

function HaloHeader(props) {
    const { t, i18n } = useTranslation();
    const [menu, useMenu] = useState('');

    const [address, useAddress] = useState();

    const [name, useName] = useState('/');

    const router = () => {
        let name = props.location.pathname;

        if (name == '/') {
            useName('/');
        } else {
            useName(name.split('/')[1]);
        }
    };

    useEffect(() => {
        router();
    }, [name, props.location.pathname]);
    useEffect(() => {
        (async () => {
            let account = await sendWeb3.eth.getAccounts();
            useAddress(account[0]);
        })();
    }, [sendWeb3]);

    const changeMenu = e => {
        message.destroy();
        console.log(name + '-------------name');
        if (
            e.key == '/' ||
            e.key == 'users' ||
            e.key == 'addUsers' ||
            e.key == 'updateUsers' ||
            e.key == 'mint' ||
            e.key == 'merit' ||
            e.key == 'setlevelMerit'


        ) {

            if (!global.isPC) {
                let eleMenu = document.querySelectorAll('.mobile-menu');
                let eleDrawer = document.querySelectorAll('.drawer-mask');
                eleMenu[0].classList.add('show-menu');
                eleMenu[0].classList.remove('show-menu');
                eleDrawer[0].classList.remove('drawer-show');
            }


            props.history.push(e.key);
            localStorage.setItem('MeuKey', e.key);

        }
    };

    const openMenu = e => {
        let eleMenu = document.querySelectorAll('.mobile-menu');
        let eleDrawer = document.querySelectorAll('.drawer-mask');
        eleMenu[0].classList.add('show-menu');
        if (e) {
            eleMenu[0].classList.add('show-menu');
            eleDrawer[0].classList.add('drawer-show');
        } else {
            eleMenu[0].classList.remove('show-menu');
            eleDrawer[0].classList.remove('drawer-show');
        }
    };
    const changeLanguages = e => {
        i18n.changeLanguage(e);
        localStorage.setItem('lang', e);
        window.location.reload();
    };


    const routerHome = () => {
        props.history.push('/');
        location.reload();
    };
    const openModal = async () => {
        if (window.ethereum) {
            window.ethereum
                .request({ method: 'eth_requestAccounts' })
                .then(accounts => {
                    if (accounts[0]) {
                        useAddress(accounts[0]);
                    }
                    sessionStorage.setItem('walletType', 'Metamask');
                })
                .catch(err => {
                    console.error(err);
                });
        }
    };

    return props.isPC ? (
        <div className="halo-header other-header">

            {
                location.hash == '#/' ?
                    '' :
                    <header className="home-hd">
                        <div>
                            {/* <img
                        className="logo-img"
                        src={logoImg}
                        onClick={changeMenu.bind(this, { key: '/' })}
                    ></img> */}
                        </div>
                        <Menu
                            mode="inline"
                            onClick={changeMenu}
                            className="hd-menu"
                            theme="dark"
                            defaultOpenKeys={[name]}
                            defaultSelectedKeys={[name]}
                            selectedKeys={[name]}
                        >
                            <SubMenu
                                className="bt"
                                title={
                                    <div>

                                        <span>绩效币</span>
                                    </div>
                                }
                                key="m"
                            >
                                <Menu.Item key="mint">绩效币</Menu.Item>

                            </SubMenu>
                            <SubMenu
                                className=""
                                title={
                                    <div>

                                        <span>员工</span>
                                    </div>
                                }
                                key="applications"
                            >
                                <Menu.Item key="setlevelMerit">设置等级基础分</Menu.Item>
                                <Menu.Item key="addUsers">添加员工</Menu.Item>
                                <Menu.Item key="updateUsers">修改员工信息</Menu.Item>
                                <Menu.Item key="users">所有员工信息</Menu.Item>
                                <Menu.Item key="merit">绩效加减分</Menu.Item>
                            </SubMenu>


                        </Menu>
                    </header>
            }


        </div>
    ) : (
        <div className="mobile-header">
            <header className="mobile-hd">
                {location.hash == '#/' ? (
                    <div className="mobile-hd-view">
                        <img src={logoImg} />
                        <div className="hd_wallet">
                            {/* <img
                                className="mobile-menu-img"
                                src={menuIcon}
                                onClick={openMenu.bind(this, true)}
                            ></img> */}
                            {!address ? (
                                <div className="ConnectWallet">
                                    <Button onClick={openModal}>{t('ConnectWallet')}</Button>
                                </div>
                            ) : (
                                <div className="wallet_address">
                                    <div>
                                        {address
                                            ? `${address.slice(0, 5)} ... ${address.slice(-5)}`
                                            : ''}
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                ) : (
                    <div className="mobile-hd-view">
                        <img src={logoImg} onClick={routerHome.bind()} />
                        <div className="hd_wallet">

                            <div>

                                {!global.$address ? (
                                    <div
                                        style={{
                                            color: '#bbb',
                                            marginRight: '10px',
                                            fontSize: '13px'
                                        }}
                                    >
                                        {' '}
                                        ● 未连接{' '}
                                    </div>
                                ) : (
                                    <div className="wallet_address">

                                        <div>
                                            {global.$address
                                                ? `${global.$address.slice(
                                                    0,
                                                    5
                                                )} ... ${global.$address.slice(-5)}`
                                                : ''}
                                        </div>

                                    </div>
                                )}
                            </div>


                            <img
                                className="mobile-menu-img"
                                src={menuIcon}
                                onClick={openMenu.bind(this, true)}
                            ></img>
                        </div>
                    </div>
                )}
            </header>


            <div className="mobile-menu">
                <div>
                    <Menu
                        mode="inline"
                        inlineCollapsed={false}
                        defaultOpenKeys={['application']}
                        onClick={changeMenu}
                        className="hd-menu"
                    >
                        <Menu.Item key="/" className="mobile-homes">

                            <span>{t('menu.home')}</span>
                        </Menu.Item>



                    </Menu>

                    {/* <a className="menu-back" onClick={openMenu.bind(this, false)}>
                    Back
                </a> */}
                </div>
            </div>
            <div className="drawer-mask" onClick={openMenu.bind(this, false)}></div>
        </div>
    );
}

export default withRouter(HaloHeader);
