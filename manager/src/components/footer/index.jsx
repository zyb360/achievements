import { useState,useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import './index.less';
import { Select } from 'antd';
import { useLocation } from 'react-router-dom';

function HaloFooter(props) {
    const { t, i18n } = useTranslation();

    const changeLanguages = e => {
        i18n.changeLanguage(e);
        localStorage.setItem('lang', e);
        window.location.reload();
    };
    const [btm,setBtm] = useState(false);
    const toDoSometing = ()=> setBtm(true);

    const { pathname } = useLocation();  // 2、存储当前路由地址
	useEffect(()=>{  // 3、通过 useEffect 监听路由变化触发事务时间
        console.log(pathname);
        if(pathname != '/'){
            toDoSometing();
        }
	},[pathname]);

    return (props.isMobile && btm) ? (
        <div className="halo-footer" id="halo-footer">
            
        </div>
    ) : (
        ''
    );
}

export default HaloFooter;
