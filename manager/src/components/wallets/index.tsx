// import { FC } from 'react';
import './index.less';

const Wallets = () => {
      return (
          <div className="wallets">
              <a href="http://hashpaywallet.com/download/index.html#/">
                  <div className="wallet">
                      <div>Hashpay</div>
                      <img src={require('@assets/Hashpay.png')} alt="Hashpay" />
                  </div>
              </a>
              <a href="https://metamask.io/">
                  <div className="wallet">
                      <div>MetaMask</div>
                      <img src={require('@assets/MetaMask.png')} alt="MetaMask" />
                  </div>
              </a>
              <a href="https://www.tokenpocket.pro/">
                  <div className="wallet">
                      <div>Token Pocket</div>
                      <img src={require('@assets/TokenPocket.png')} alt="Token Pocket" />
                  </div>
              </a>
              <a href="https://token.im/"> 
                  <div className="wallet">
                      <div>imToken</div>
                      <img
                        src={require('@assets/imToken.png')}
                        alt="imToken"
                        style={{ height: 13 }}
                      />
                  </div>
              </a>
          </div>
      );
};

export default Wallets;
