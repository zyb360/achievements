import { useState, useRef, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { message, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import './index.less';

function topMenu(props) {
    const { pathname } = useLocation();
    const { t, i18n } = useTranslation();
    const [address, setAddress] = useState(global.$address);
    const childrenRef = useRef();

    const changeLanguages = e => {
        i18n.changeLanguage(e);
        localStorage.setItem('lang', e);
        window.location.reload();
    };

    return (
        <div className="language-change" id="language-change">
           
            <div>
                <Select className='language'
                    defaultValue={
                        localStorage.getItem('lang') ? localStorage.getItem('lang') : 'en'
                    }
                    onChange={changeLanguages}
                    getPopupContainer={() =>
                        document.getElementById('language-change')
                    }
                >
                    <Select.Option value="zh_CN">{t('chinese')}</Select.Option>
                    <Select.Option value="en">{t('english')}</Select.Option>
                </Select>
            </div>
        </div>
    );
}
export default topMenu;
