// eslint-disable-next-line no-use-before-define
import React, { lazy } from 'react';
import { Route } from 'react-router-dom';

const Index = lazy(() => import('@pages/home/index'));

const users = lazy(() => import('@src/pages/admin/users/index'));

const addUsers = lazy(() => import('@src/pages/admin/addUsers/index'));
const update = lazy(() => import('@src/pages/admin/updateUsers/index'));

const mint = lazy(() => import('@src/pages/admin/mint/index'));
const merit = lazy(() => import('@src/pages/admin/merit/index'));

const setlevelMerit = lazy(() => import('@src/pages/admin/setlevelMerit/index'));

const routes = [
    {
        // 首页
        path: '/',
        component: Index
    },
    {
        // 首页
        path: '/addUsers',
        component: addUsers
    },
    {
        path:'/updateUsers',
        component:update
    },
    {
        path:'/users',
        component:users
    },
    {
        path:'/mint',
        component:mint
    },
    {
        path:'/merit',
        component:merit
    },
    {
        path:'/setlevelMerit',
        component:setlevelMerit
    }

];

const rootConfig = routes.map(route => {
    const { component: Component } = route;
    return <Route key={route.path} path={route.path} exact component={Component} />;
});
export default rootConfig;
