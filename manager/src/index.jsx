import { Suspense, useState, useRef } from 'react';
import ReactDOM from 'react-dom';
import { ConfigProvider, Layout, message } from 'antd';
import 'dayjs/locale/zh-cn';
import { Switch, Router, Route, Redirect } from 'react-router-dom';
import { createHashHistory } from 'history';
import { setSessionStorage, getSessionStorage, sendWeb3,callWeb3, sendContracts } from './utils';
import Home from '@src/pages/my/index';
import HaloHeader from '@components/header/index';
import HaloFooter from '@components/footer/index';
import routeConfig from '@routeConfig';
import './styles/index.less';
import TopMenu from './components/header/topMenu/index.jsx';
import zhCN from 'antd/es/locale/zh_CN';
import './i18n';
import { useTranslation } from 'react-i18next';
const { Content } = Layout;
const history = createHashHistory();
const APP = () => {
    const { t } = useTranslation();
    const [isPC, useisPC] = useState(document.documentElement.offsetWidth > 1200);
    const [chainIdTwo, useChainIdTwo] = useState(0);
    global.isPC = isPC;
    global.t = t;

    const web3 = async () => {
       
        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                global.$address = res[0];
            });
        }
    };
    web3();

    global.onresize = () => {
        useisPC(document.documentElement.offsetWidth > 1200);
        global.isPC = isPC;
    };
    if (global.ethereum) {
        global.ethereum.on('accountsChanged', accounts => {
            if (accounts[0]) {
                global.$address = accounts[0];
            }
            location.reload(true);
        });
        global.ethereum.on('networkChanged', networkChanged => {
            location.reload(true);
        });
    }
    sendWeb3.eth.getChainId().then(async chainId => {
        global.chainId = chainId;
        useChainIdTwo(chainId);
    });

    return (
        <ConfigProvider locale={zhCN}>
            <Router history={history}>
                <Layout className="">
                    <TopMenu></TopMenu>
                    <Layout className={location.hash == '#/'? '':'homes-layout'}>
                        <HaloHeader isPC={isPC} />
                        <Content>
                            <Suspense fallback="  ">
                                <Switch>
                                    {routeConfig}
                                    <Redirect from="/*" to="/" />
                                    <Route path="/" exact component={Home} />
                                </Switch>
                            </Suspense>
                        </Content>
                    </Layout>
                </Layout>
                <Layout>
                    <HaloFooter isPC={isPC} />
                </Layout>
            </Router>
        </ConfigProvider>
    );
};

ReactDOM.render(<APP />, document.getElementById('root'));
