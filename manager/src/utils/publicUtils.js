/*eslint-disable*/
import { message } from 'antd';
import { sendWeb3, sendContracts } from '@utils/index';

// 提示
export function messageWay(val, msg) {
    let styles = {
        marginTop: '28vh'
        // marginLeft: global.isMobile ? '240px' : ''
    };
    if (msg == 'success') {
        message.success({
            content: val,
            key: 'id_1', //可以通过唯一的 key 来更新loading的状态，否则会弹出两个框。
            duration: 2,
            style: styles
        });
    } else if (msg == 'error') {
        message.error({
            content: val,
            duration: 1.5,
            key: 'id_1',
            style: styles
        });
    } else if (msg == 'loading') {
        const hide = message.loading({
            content: val,
            key: 'id_1',
            duration: 100,
            style: styles
        });
        setTimeout(hide, 2500);
    }
}

export function convertAmount(amountText, decimas) {
    let reg = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

    let parts = amountText.split('.');
    if (!reg.test(amountText)) {
        messageWay('Please enter the correct number', 'error'); //请输入正确的数字
        throw 'Invaild Amount String';
    }

    let intergralPart = parts[0];
    let floatPart = parts[1];
    if (floatPart == undefined) {
        floatPart = '';
    }
    if (floatPart.length > decimas) {
        floatPart = floatPart.slice(0, decimas);
    }

    let toBN = sendWeb3.utils.toBN;

    let ret = toBN(intergralPart + floatPart).mul(
        toBN('10').pow(toBN(parseInt(decimas) - floatPart.length))
    );

    console.log(`Origin:${amountText} -> ${ret.toString()}`);
    return ret.toString();
}

//向后补0
export function supplementWay(val) {
    let value = parseFloat(val);
    let xsd = value.toString().split('.');
    if (xsd.length == 1) {
        value = value.toString() + '.000000';
        return value;
    }
    if (xsd.length > 1) {
        if (xsd[1].length < 2) {
            value = value.toString() + '00000';
        } else if (xsd[1].length < 3) {
            value = value.toString() + '0000';
        } else if (xsd[1].length < 4) {
            value = value.toString() + '000';
        } else if (xsd[1].length < 5) {
            value = value.toString() + '00';
        } else if (xsd[1].length < 6) {
            value = value.toString() + '0';
        } else if (xsd[1].length >= 6) {
            value = value.toString();
        }
        return value;
    }
}

//向后补两0
export function supplementWayTwo(val) {
    let value = val;
    let xsd = value.split('.');
    if (xsd.length == 1) {
        value = value.toString() + '.00';
        return value;
    }
    if (xsd.length > 1) {
        if (xsd[1].length < 2) {
            value = value.toString() + '0';
        }
        return value;
    }
}

//对时间进行转换
export function dateTimeWay(val) {
    let date = new Date(Number(val * 1000));
    let y = date.getFullYear();
    let m = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
    let d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    let md = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    return y + '.' + m + '.' + d;
}

export function dateTimeWays(val) {
    let date = new Date(Number(val * 1000));
    let y = date.getFullYear();
    let m = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
    let d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    let md = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    return y + '/' + m + '/' + d;
}

export function dateSubMonth(date) {
    var date1 = new Date(Number(date * 1000));
    var date2 = new Date();
    var monthCount = parseInt(date2.getFullYear() - date1.getFullYear()) * 12 - date1.getMonth() + date2.getMonth();
    var resM = monthCount % 12;
    var resY = parseInt(monthCount / 12);
    var resStr = "";

    resStr += resY + "年"

    if (resM != 0) {
        resStr += resM + "月"
    }
    if (resStr == "") {
        resStr = "0";
    }
    return resStr;
}

