/* eslint-disable no-undef */
import Web3 from 'web3';
import { message } from 'antd';
import Contracts from './contracts.json';
import erc20 from '../../contracts/MeritToken.json';
const sendWeb3 = new Web3(window.ethereum);
const keys = Object.keys(Contracts),
    callContracts = {},
    sendContracts = {};

for (const key of keys) {
    sendContracts[key] = new sendWeb3.eth.Contract(Contracts[key].abi, Contracts[key].address);
    
}
sendContracts['ERC20'] = new sendWeb3.eth.Contract(erc20.abi,'0xeFf9F3F01D118A01ABDb6D8eCFA4ADeddA861A96');


export { callContracts, sendContracts, sendWeb3 };


export function setSessionStorage(key, value) {
    sessionStorage.setItem(`${key}`, value);
}

export function getSessionStorage(key) {
    return sessionStorage.getItem(`${key}`);
}
// 精度转换
function toWei(unit = 'ether') {
    return Web3.utils.toWei(`${this}`, unit);
}

function fromWei(unit = 'ether') {
    return Web3.utils.fromWei(this, unit);
}

function toBN() {
    return new Web3.utils.BN(this);
}

function toFixed(decimals = 2) {
    const _this = typeof this !== 'string' ? `${this}` : this;
    if (!_this.includes('.')) return _this;

    const index = _this.indexOf('.');
    if (_this.slice(index + 1).length <= 2) return _this;

    return parseFloat(_this.slice(0, index + 1 + decimals));
}

String.prototype.toWei = toWei;
String.prototype.fromWei = fromWei;
String.prototype.toFixed = toFixed;
String.prototype.toBN = toBN;
Number.prototype.toWei = toWei;
Number.prototype.fromWei = fromWei;
Number.prototype.toFixed = toFixed;
