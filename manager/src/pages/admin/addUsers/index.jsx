import { Component } from 'react';
import { Button, Select, Input, Table } from 'antd';
const { Option } = Select;
const { Column } = Table;
import { sendWeb3, sendContracts } from '@utils/index';

import { messageWay,dateTimeWays } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            user: '',
            userName: '',
            level: '',
            loading: false,
            user1: '',
            lists: []

        };
    }
    async componentDidMount() {
        await this.init();
    }
    init = async () => {

        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
        

    }
    setUser = async (e) => {
        this.setState({
            user: e.target.value
        });

    }
    setUserName = async (e) => {
        this.setState({
            userName: e.target.value
        });

    }
    setLevel = async (e) => {
        this.setState({
            level: e.target.value
        });

    }
    cancel = () => {
        this.setState({
            user: '',
            userName: '',
            level: '',
            loading: false

        });
    }
    //0x436F8c113076a456c314142211a4DFEd910f668F
    submit = () => {

       if(!sendWeb3.utils.isAddress(this.state.user)){
        return messageWay('请输入正确的地址','error');
       } 
        let reg=[1,2,3,4,5,6];
       if(!reg.includes(Number(this.state.level))){
          return messageWay('等级请输入1-6','error');
       };


        this.setState({
            loading: true
        });
        sendContracts.MeritFactory.methods.addUser(
            this.state.user, this.state.userName, this.state.level
        ).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('添加成功', 'success');
            this.init();
            this.cancel();
        }).on('error', err => {
            messageWay('取消', 'error');
            this.setState({
                loading: false
            });
        });
    }
    setUser1 = async (e) => {
      await  this.setState({
            user1: e.target.value
        });

    }
    history = async () => {
        let data = [];

        let info = await sendContracts.MeritFactory.methods.users(this.state.user1).call();
        console.log(info);
        data.push({
            user:this.state.user1,
            name:info.name,
            level:info.level,
            created_at: dateTimeWays(info.created_at),
            eventType: info.eventType,
            explain: info.explain,
            status:info.status
        });
        this.setState({
            lists:data
        });
    }
    render() {
        const state = this.state;
        return (
            <div className='ido'>

                <div className='box' >
                    <div className='title'>添加员工</div>

                    <div className='item' id='language-change'>
                        <div className='other-row'>
                            <span>员工地址：</span>
                            <Input onInput={this.setUser} value={state.user}></Input>
                        </div>
                        <div className='other-row'>
                            <span>姓名：</span>
                            <Input onInput={this.setUserName} value={state.userName} maxLength='40'></Input>
                        </div>
                        <div className='other-row'>
                            <span>等级：</span>
                            <Input placeholder="请输入1-6" onInput={this.setLevel} value={state.level}></Input>
                        </div>

                        <Button loading={state.loading} className='same-btn' onClick={this.submit}>添加</Button>
                    </div>
                </div>
                <div className='box' >
                    <div className='title'>查询员工信息</div>
                    <div>
                        <span>员工地址：</span>
                        <Input onInput={this.setUser1} value={state.user1} style={{ width: '400px', marginRight: '10px' }}></Input>
                        <Button className='same-btn' onClick={this.history}>确定</Button>
                    </div>
                    <div className='item'>
                        <Table dataSource={state.lists} size="middle" rowKey={'name'}>
                            <Column
                                title="姓名"
                                dataIndex="name"
                                key="name"
                            ></Column>
                            <Column
                                title="地址"
                                dataIndex="user"
                                key="user"
                                render={(e, row) => (
                                    <div>{row.user.slice(0, 4)}...{row.user.slice(-4)}</div>
                                )}
                            ></Column>
                            <Column
                                title="等级"
                                dataIndex="level"
                                key="level"
                            ></Column>
                            <Column
                                title="入职时间"
                                dataIndex="created_at"
                                key="created_at"
                            ></Column>
                            <Column
                                title="状态"
                                dataIndex="status"
                                key="status"
                                render={
                                    (e, row) => (
                                        <div className={row.status == 0 ? '' : 'red'}>{row.status == 0 ? '在职' : '离职'}</div>
                                    )
                                }
                            ></Column>
                        </Table>
                    </div>


                </div>

            </div>
        );
    }
}
