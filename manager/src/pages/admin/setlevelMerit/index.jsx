import { Component } from 'react';
import { Button, Select, Input } from 'antd';
const { Option } = Select;
import { sendWeb3, sendContracts } from '@utils/index';

import { messageWay } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            user: '',
            amount: '',
            level: '',
            loading: false,
            loading1: false,
            lists: []

        };
    }
    async componentDidMount() {
        await this.init();
    }
    init = async () => {

        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
       

        let totalGrade = await sendContracts.MeritFactory.methods.totalGrade().call();
        console.log(totalGrade);

        let data = [];
        for (let index = 1; index <= 6; index++) {
            let grade = await sendContracts.MeritFactory.methods.grade(index).call();
            data.push(grade);
        }
        this.setState({
            lists: data
        });

    }
    setAmount = async (e) => {
        this.setState({
            amount: e.target.value
        });

    }

    setLevel = async (e) => {
      await  this.setState({
            level: e.target.value
        });

    }
    cancel = () => {
        this.setState({
            user: '',
            amount: '',
            level: '',
            loading: false,
            loading1: false
        });
    }

    //0x436F8c113076a456c314142211a4DFEd910f668F
    submit = () => {

       let reg=[1,2,3,4,5,6];
       if(!reg.includes(Number(this.state.level))){
          return messageWay('等级请输入1-6','error');
       };
       
       if(isNaN(Number(this.state.amount))){
        return messageWay('基础分只能输入数字','error');
       }

        this.setState({
            loading: true
        });
        sendContracts.MeritFactory.methods.setGrade(
            this.state.level, this.state.amount
        ).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('设置成功', 'success');
            this.init();
            this.cancel();
        }).on('error', err => {
            messageWay('取消', 'error');
            this.setState({
                loading: false
            });
        });
    }

    render() {
        const state = this.state;
        return (
            <div className='ido'>
                <div className='box'>
                    {
                        state.lists.map((item, key) => {
                            return (
                                <p key={key}>Q{key + 1}基础分： {item}</p>
                            );
                        })
                    }
                </div>

                <div className='box' >
                    <div className='title'>等级基础分</div>
                    <div className='item'>

                        <div className='other-row'>
                            <span>等级：</span>
                            <Input placeholder="请输入1-6" onInput={this.setLevel} value={state.level}></Input>
                        </div>
                        <div className='other-row'>
                            <span>基础分：</span>
                            <Input onInput={this.setAmount}  value={state.amount} maxLength='3'></Input>
                        </div>
                    </div>
                    <div className='item' >
                        <Button loading={state.loading} className='same-btn' onClick={this.submit}>设置</Button>
                    </div>
                </div>

            </div>
        );
    }
}
