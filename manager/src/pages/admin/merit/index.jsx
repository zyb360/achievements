import { Component } from 'react';
import { Button, Select, Input, Table } from 'antd';
const { Option } = Select;
const { Column } = Table;
import { sendWeb3, sendContracts } from '@utils/index';

import { messageWay, dateTimeWays } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            user: '',
            userName: '',
            amount: '',
            loading: false,
            loading1: false,
            desc: '',
            amount1: '',
            desc1: '',
            lists: []

        };
    }
    async componentDidMount() {
        await this.init();
    }
    init = async () => {

        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
        console.log(sendContracts);
    }
    history = async () => {
        let len = await sendContracts.ERC20.methods.userLogLength(this.state.user).call();
        console.log(len);

        let datas = [];
        if (len > 0) {
            let userLogData = await sendContracts.ERC20.methods.userLogData(this.state.user, 0, len).call();
            for (let index = 0; index < userLogData.length; index++) {
                datas.push({
                    amount: userLogData[index].amount.fromWei(),
                    created_at: dateTimeWays(userLogData[index].created_at),
                    eventType: userLogData[index].eventType,
                    explain: userLogData[index].explain
                });

            }
            console.log(userLogData);
            this.setState({
                lists: datas
            });
        }
    }
    setUser = async (e) => {
        this.setState({
            user: e.target.value
        });

    }

    setAmount = async (e) => {
        this.setState({
            amount: e.target.value
        });

    }
    setDesc = async (e) => {
        this.setState({
            desc: e.target.value
        });

    }
    setAmount1 = async (e) => {
        this.setState({
            amount1: e.target.value
        });

    }
    setDesc1 = async (e) => {
        this.setState({
            desc1: e.target.value
        });

    }
    cancel = () => {
        this.setState({
            user: '',
            amount: '',
            amount1: '',
            desc1: '',
            loading: false,
            loading1: false
        });
    }

    //0x436F8c113076a456c314142211a4DFEd910f668F
    submit = () => {

        if (Number(this.state.amount) > 100) {
            return messageWay('请输入小于100的数', 'error');
        }
        let reg = /^\d+(\.\d{1,1})?$/;
        let isMatch = reg.test(this.state.amount);
        if (!isMatch) {
            return messageWay('最多填写小数点后1位', 'error');
        }

        this.setState({
            loading: true
        });
        sendContracts.ERC20.methods.mint(
            this.state.user, this.state.amount.toWei(), this.state.desc
        ).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('加分成功', 'success');
            this.init();
            this.cancel();
        }).on('error', err => {
            messageWay('取消', 'error');
            this.setState({
                loading: false
            });
        });
    }
    submit1 = () => {
        if (Number(this.state.amount1) > 100) {
            return messageWay('请输入小于100的数', 'error');
        }
        let reg = /^\d+(\.\d{1,1})?$/;
        let isMatch = reg.test(this.state.amount1);
        if (!isMatch) {
            return messageWay('最多填写小数点后1位', 'error');
        }
        this.setState({
            loading1: true
        });
        sendContracts.ERC20.methods.burnFrom(
            this.state.user, this.state.amount1.toWei(), this.state.desc1
        ).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('减分成功', 'success');
            this.init();
            this.cancel();
        }).on('error', err => {
            messageWay('取消', 'error');
            this.setState({
                loading1: false
            });
        });
    }
    render() {
        const state = this.state;
        return (
            <div className='ido'>
                <div className='box' >
                    <div className='title'>加分</div>
                    <div className='item'>
                        <div className='other-row'>
                            <span>员工地址：</span>
                            <Input onInput={this.setUser} value={state.user}></Input>
                        </div>
                        <div className='other-row'>
                            <span>数量：</span>
                            <Input onInput={this.setAmount} value={state.amount}></Input>
                        </div>
                        <div className='other-row'>
                            <span>备注：</span>
                            <Input onInput={this.setDesc} value={state.desc} maxLength='16'></Input>
                        </div>
                    </div>

                    <div className='item'>
                        <Button loading={state.loading} className='same-btn' onClick={this.submit}>加分</Button>
                    </div>
                </div>

                <div className='box' >
                    <div className='title'>减分</div>
                    <div className='item'>
                        <div className='other-row'>
                            <span>员工地址：</span>
                            <Input onInput={this.setUser} value={state.user}></Input>
                        </div>
                        <div className='other-row'>
                            <span>数量：</span>
                            <Input onInput={this.setAmount1} value={state.amount1}></Input>
                        </div>
                        <div className='other-row'>
                            <span>备注：</span>
                            <Input onInput={this.setDesc1} value={state.desc1} maxlength='16'></Input>
                        </div>
                    </div>

                    <div className='item' >
                        <Button loading={state.loading1} className='same-btn' onClick={this.submit1}>减分</Button>
                    </div>
                </div>

                <div className='box'>
                    <div className='title'>记录</div>
                    <div>
                        <span>员工地址：</span>
                        <Input onInput={this.setUser} value={state.user} style={{ width: '400px', marginRight: '10px' }}></Input><Button className='same-btn' onClick={this.history}

                        >确定</Button>
                    </div>
                    <div className='item'>
                        <Table dataSource={state.lists} size="middle" >
                            <Column

                                title="日期"
                                dataIndex="created_at"
                                key="created_at"
                            ></Column>
                            <Column
                                title="事项"
                                dataIndex="explain"
                                key="explain"
                            ></Column>
                            <Column
                                title="分值"
                                dataIndex="amount"
                                key="amount"
                                render={(e, row) => (
                                    <div className={row.eventType ? '' : 'red'}>{row.eventType ? '+' : '-'}
                                        {row.amount}</div>
                                )}
                            ></Column>

                        </Table>
                    </div>
                </div>

            </div>
        );
    }
}
