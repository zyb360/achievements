import { Component } from 'react';
import { Button, Select, Input } from 'antd';
const { Option } = Select;
import { sendWeb3, sendContracts } from '@utils/index';

import { messageWay } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            user: '',
            userName: '',
            oldLevel: '',//当前等级
            level: '',
            loading: false,
            loading1: false,
            loading2: false

        };
    }
    async componentDidMount() {
        await this.init();
    }
    init = async () => {

        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }

    }
    setUser = async (e) => {
        await this.setState({
            user: e.target.value
        });

        let info = await sendContracts.MeritFactory.methods.users(e.target.value).call();
        this.setState({
            oldLevel: info.level
        });

    }

    setLevel = async (e) => {
        this.setState({
            level: e.target.value
        });
    }
    cancel = () => {
        this.setState({
            user: '',
            userName: '',
            level: '',
            loading: false,
            loading1: false,
            loading2: false
        });
    }

    //0x436F8c113076a456c314142211a4DFEd910f668F
    submit = () => {
        let reg = [1, 2, 3, 4, 5, 6];
        if (!reg.includes(Number(this.state.level))) {
            return messageWay('请输入1-6', 'error');
        };

        this.setState({
            loading: true
        });
        sendContracts.MeritFactory.methods.updateUserLevel(
            this.state.user, this.state.level
        ).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('修改成功', 'success');
            this.init();
            this.cancel();
        }).on('error', err => {
            messageWay('取消', 'error');
            this.setState({
                loading: false
            });
        });
    }
    submit1 = () => {
        this.setState({
            loading1: true
        });
        sendContracts.MeritFactory.methods.removeUser(
            this.state.user
        ).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('离职成功', 'success');
            this.init();
            this.cancel();
        }).on('error', err => {
            messageWay('取消', 'error');
            this.setState({
                loading1: false
            });
        });
    }
    submit2 = () => {
        let reg = [1, 2, 3, 4, 5, 6];
        if (!reg.includes(Number(this.state.level))) {
            return messageWay('请输入1-6', 'error');
        };
        this.setState({
            loading2: true
        });
        sendContracts.MeritFactory.methods.againUser(
            this.state.user, this.state.level
        ).send({
            from: this.state.address
        }).on('receipt', res => {
            messageWay('离职成功', 'success');
            this.init();
            this.cancel();
        }).on('error', err => {
            messageWay('取消', 'error');
            this.setState({
                loading2: false
            });
        });
    }
    render() {
        const state = this.state;
        return (
            <div className='ido'>
                <div className='box' >
                    <div className='title'>修改用户等级</div>
                    <div className='item'>
                        <div className='other-row'>
                            <span>员工地址：</span>
                            <Input onInput={this.setUser} value={state.user}></Input>
                        </div>
                        <div className='other-row'>
                            <span>等级：{state.oldLevel}</span>
                            <Input placeholder="请输入1-6" onInput={this.setLevel} value={state.level}></Input>
                        </div>
                    </div>

                    <div className='item' id='language-change'>
                        <Button loading={state.loading} className='same-btn' onClick={this.submit}>修改</Button>
                    </div>
                </div>
                <div className='box' >
                    <div className='title'>离职</div>
                    <div className='item'>
                        <div className='other-row'>
                            <span>员工地址：</span>
                            <Input onInput={this.setUser} value={state.user}></Input>
                        </div>

                    </div>

                    <div className='item' id='language-change'>
                        <Button loading={state.loading1} className='same-btn' onClick={this.submit1}>离职</Button>
                    </div>
                </div>
                <div className='box' >
                    <div className='title'>离职入职</div>
                    <div className='item'>
                        <div className='other-row'>
                            <span>员工地址：</span>
                            <Input onInput={this.setUser} value={state.user}></Input>
                        </div>
                        <div className='other-row'>
                            <span>等级：{state.oldLevel}</span>
                            <Input placeholder="请输入1-6" onInput={this.setLevel} value={state.level}></Input>
                        </div>

                    </div>

                    <div className='item' id='language-change'>
                        <Button loading={state.loading2} className='same-btn' onClick={this.submit2}>重新入职</Button>
                    </div>
                </div>
            </div>
        );
    }
}
