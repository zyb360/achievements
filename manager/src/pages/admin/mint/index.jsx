import { Component } from 'react';
import { Button, Select, Input } from 'antd';
const { Option } = Select;
import { sendWeb3, sendContracts } from '@utils/index';

import { messageWay } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            user: '',
            userName: '',
            amount: '',
            loading: false,
            loading1: false

        };
    }
    async componentDidMount() {
        await this.init();
    }
    init = async () => {

        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
        

        let totalGrade = await sendContracts.ERC20.methods.year().call();
        console.log(totalGrade);

    }
    setUser = async (e) => {
        this.setState({
            user: e.target.value
        });

    }

    setAmount = async (e) => {
        this.setState({
            amount: e.target.value
        });

    }
    cancel = () => {
        this.setState({
            user: '',
            amount: '',
            loading: false,
            loading1: false
        });
    }

    //0x436F8c113076a456c314142211a4DFEd910f668F
    submit = async() => {
       
        let year=await sendContracts.MeritFactory.methods.tokenYear(
            this.state.amount
        ).call();
        this.setState({
            user:year
        });
    }

    render() {
        const state = this.state;
        return (
            <div className='ido'>
                <div className='box' >
                    <div className='title'>绩效币地址  {state.user}</div>
                    <div className='item'>
                        <div className='other-row'>
                            <span>每年(天数)：</span>
                            <Input onInput={this.setAmount} value={state.amount}></Input>
                        </div>
                    </div>

                    <div className='item' id='language-change'>
                        <Button  className='same-btn' onClick={this.submit}>查看</Button>
                    </div>
                </div>
                
            </div>
        );
    }
}
