import { Component } from 'react';
import { Button, Select, Input, Table } from 'antd';
const { Option } = Select;
const { Column } = Table;
import { sendWeb3, sendContracts } from '@utils/index';
import { CSVLink } from 'react-csv';
import { messageWay, dateTimeWays } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            user: '',
            userName: '',
            level: '',
            loading: false,
            loading1: false,
            lists: [

            ],
            oldLists: [],
            total: 0,
            downloadLists: [],
            headers: [
                { label: '姓名', key: 'name' },
                { label: '地址', key: 'user' },
                { label: '等级', key: 'level' },
                { label: '入职时间', key: 'created_at' },
                { label: '状态', key: 'status' },
                { label: '绩效分', key: 'ownerMerits' }
            ]

        };
    }
    async componentDidMount() {
        await this.init();
    }
    init = async () => {

        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
        console.log(sendContracts);

        let users = await sendContracts.MeritFactory.methods.allUserAddress().call();

        if (users.length > 0) {
            this.setState({
                total: users.length
            });
            let data = [];
            let data1 = [];
            for (let index = 0; index < users.length; index++) {
                let info = await sendContracts.MeritFactory.methods.users(users[index]).call();
                console.log(info);

                let balanceOf = await sendContracts.ERC20.methods.balanceOf(users[index]).call();
                let grade = await sendContracts.MeritFactory.methods.grade(info.level).call(); //基础分
                let result = Number(balanceOf.fromWei()) + Number(grade);

                data.push({
                    key: index + 1,
                    name: info.name,
                    user: users[index],
                    level: info.level,
                    created_at: info.created_at,
                    status: info.status,
                    ownerMerits: result
                });

                data1.push({
                    key: index + 1,
                    name: info.name,
                    user: users[index],
                    level: info.level,
                    created_at: dateTimeWays(info.created_at),
                    status: info.status == 0 ? '在职' : '离职',
                    ownerMerits: result
                });
            }
            this.setState({
                lists: data,
                oldLists: data,
                downloadLists: data1
            });
        }

    }
    setUser = async (e) => {
        await this.setState({
            user: e.target.value
        });

        let result = this.state.oldLists.filter(item => item.user == e.target.value);
        this.setState({
            lists: result
        });
    }

    setLevel = async (e) => {
        this.setState({
            level: e.target.value
        });

    }
    cancel = () => {
        this.setState({
            user: '',
            userName: '',
            level: '',
            loading: false,
            loading1: false
        });
    }



    render() {
        const state = this.state;
        return (
            <div className='ido'>
                <div className='box' >
                    <div className='title'>员工信息</div>
                    <div>
                        <span>员工地址：</span>
                        <Input onInput={this.setUser} value={state.user} style={{ width: '400px', marginRight: '10px' }}></Input>
                        {/* <Button className='same-btn' onClick={this.history}>确定</Button> */}


                    </div>
                    <div>

                    </div>
                    <div className='item'>
                        <Table dataSource={state.lists} size="middle" showSorterTooltip="false"
                            pagination={{
                                showSizeChanger: true,
                                pageSizeOptions: [10, 20, 50],
                                showQuickJumper: true//快速到第几页

                            }}
                        >
                            <Column
                                title="姓名"
                                dataIndex="name"
                                key="name"
                            ></Column>
                            <Column
                                title="地址"
                                dataIndex="user"
                                key="user"
                                render={(e, row) => (
                                    <div>{row.user.slice(0, 4)}...{row.user.slice(-4)}</div>
                                )}
                            ></Column>
                            <Column
                                title="等级"
                                dataIndex="level"
                                key="level"


                                sorter={(a, b) => a.level - b.level}
                            ></Column>
                            <Column
                                title="入职时间"
                                dataIndex="created_at"
                                key="created_at"

                                sorter={(a, b) => a.created_at - b.created_at}
                                render={(e, row) => (
                                    <div>{dateTimeWays(row.created_at)}</div>
                                )}
                            ></Column>
                            <Column
                                title="状态"
                                dataIndex="status"
                                key="status"
                                render={
                                    (e, row) => (
                                        <div className={row.status == 0 ? '' : 'red'}>{row.status == 0 ? '在职' : '离职'}</div>
                                    )
                                }
                            ></Column>
                            <Column
                                title="绩效分"
                                dataIndex="ownerMerits"
                                key="ownerMerits"

                                sorter={(a, b) => a.ownerMerits - b.ownerMerits}
                            ></Column>
                        </Table>
                        {
                            state.downloadLists.length > 0 ?
                                <CSVLink className='download'
                                    filename="员工表.csv"
                                    headers={state.headers}
                                    data={state.downloadLists}

                                >下载CSV</CSVLink > : ''
                        }

                    </div>


                </div>

            </div>
        );
    }
}
