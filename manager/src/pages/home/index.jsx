import { Component } from 'react';
import { Row, Col, DatePicker, Tooltip } from 'antd';

import bg1 from '@assets/home/bg1.png';
import bg2 from '@assets/home/bg2.png';
import q1 from '@assets/home/q1.png';
import q2 from '@assets/home/q2.png';
import q3 from '@assets/home/q3.png';
import q4 from '@assets/home/q4.png';
import q5 from '@assets/home/q5.png';
import q6 from '@assets/home/q6.png';
import downImg from '@assets/home/down.png';
import timeImg from '@assets/home/time.png';
import LocalFormat from '@utils/localHelper.json';
import './index.less';
import { sendWeb3, sendContracts } from '@src/utils';
import { dateTimeWays, dateSubMonth } from '@src/utils/publicUtils.js';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            userName: 'admin',
            level: '',
            levelBg: q2,
            fraction: '',//分数
            TotalScore: '',//总分
            timeSlot: 0,
            bannerContent: '',
            ownerMerits: 0,
            totalMerits: 0,
            seniority: '0',
            isShow1: false,
            isShow2: false,
            datePickerOpen: false,
            lists: [],
            oldLists: []
        };
    }
    componentDidMount() {

        this.init();

        let timeCount;
        // window.addEventListener('scroll', function () {
        //     if (this.state.isLoadingMore) {
        //         return;
        //     }

        //     if (timeCount) {
        //         clearTimeout(timeCount);
        //     }

        //     timeCount = setTimeout(this.callback(), 10);
        // }.bind(this), false);

    }
    init = async () => {
        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
        let data = new Date();
        let h = data.getHours();
        console.log(h);

        console.log(h);
        let desc = '';
        if (h >= 7 && h <= 11) {
            desc = '上午好，又是元气满满的一天哟~';
        } if (h > 11 && h <= 13) {
            desc = '中午好，努力工作~努力干饭！';
        }
        if (h >= 14 && h <= 18) {
            desc = '下午好，记得多喝热水~';
        } if (h > 18 && h < 22) {

            desc = '晚上好，早睡早起身体好。';
            this.setState({
                timeSlot: 1
            });
        }

        this.setState({
            bannerContent: desc
        });


        let info = await sendContracts.MeritFactory.methods.users(this.state.address).call();
        console.log(info);
        let grade = await sendContracts.MeritFactory.methods.grade(info.level).call(); //基础分
        this.setState({
            userName: info.name,
            level: info.level
        });

        let balanceOf = await sendContracts.ERC20.methods.balanceOf(this.state.address).call();
        let totalSupply = await sendContracts.ERC20.methods.totalSupply().call();
        let totalGrade = await sendContracts.MeritFactory.methods.totalGrade().call();

        this.setState({
            ownerMerits: Number(balanceOf.fromWei()) + Number(grade),
            totalMerits: Number(totalSupply.fromWei()) + Number(totalGrade)
        });

        if (info.created_at != '0') {
            let year = dateSubMonth(info.created_at);
            this.setState({
                seniority: year
            });
        }

        switch (info.level) {
            case '1':
                this.setState({
                    levelBg: q1
                });
                break;
            case '2':
                this.setState({
                    levelBg: q2
                });
                break;
            case '3':
                this.setState({
                    levelBg: q3
                });
                break;
            case '4':
                this.setState({
                    levelBg: q4
                });
                break;
            case '5':
                this.setState({
                    levelBg: q5
                });
                break;
            case '6':
                this.setState({
                    levelBg: q6
                });
                break;
            default:
                break;
        }

        let len = await sendContracts.ERC20.methods.userLogLength(this.state.address).call();
        console.log(len);

        let datas = [];
        if (len > 0) {
            let userLogData = await sendContracts.ERC20.methods.userLogData(this.state.address, 0, len).call();
            for (let index = 0; index < userLogData.length; index++) {
                datas.push({
                    amount: userLogData[index].amount.fromWei(),
                    created_at: dateTimeWays(userLogData[index].created_at),
                    eventType: userLogData[index].eventType,
                    explain: userLogData[index].explain
                });

            }
            console.log(userLogData);
            this.setState({
                lists: datas,
                oldLists: datas
            });
        }
    }

    handleScroll = () => {


        // const that = this; // 为解决不同context的问题
        // const top = wrapper.getBoundingClientRect().top;
        // console.log(top);
        // const windowHeight = window.screen.height;
        // console.log(windowHeight);
        // if (top && top < windowHeight) {
        //     // 当 wrapper 已经被滚动到页面可视范围之内触发
        //     this.loadMoreDataFn(that);
        // }
        console.log(this.myRef.scrollHeight);
        console.log(this.myRef.clientHeight);
        console.log(this.myRef.scrollTop);
        let res = this.myRef.scrollHeight - this.myRef.clientHeight - this.myRef.scrollTop;
        console.log(res);
        if (res > 1) {
            //未到底
        } else {
            //已到底部
            this.loadMoreDataFn();
        }

    }
    loadMoreDataFn = (that) => {
        return;
        if (this.myRef.scrollTop > 1000) return;
        this.setState({
            lists: this.state.lists.concat([{
                time: '2018/08/16',
                desc: '月度加分',
                score: '+5'
            }, {
                time: '2018/08/16',
                desc: '月度加分',
                score: '+5'
            }, {
                time: '2018/08/16',
                desc: '月度加分',
                score: '+5'
            }, {
                time: '2018/08/16',
                desc: '月度加分',
                score: '+5'
            }])
        });
    }

    datePickerOpen = () => {
        this.setState({
            datePickerOpen: !this.state.datePickerOpen
        });
    }
    onChange = (value, dateString) => {
        console.log(value);
        console.log(dateString);
        this.setState({
            datePickerOpen: !this.state.datePickerOpen
        });

        let data = this.state.oldLists.filter(item => item.created_at == dateString);
        this.setState({
            lists: data
        });
    }
    open1 = () => {
        this.setState({
            isShow1: true,
            isShow2: false
        });
    }
    set1 = () => {
        this.setState({
            isShow1: false
        });
    }
    open2 = () => {
        this.setState({
            isShow2: true,
            isShow1: false
        });
    }
    set2 = (e) => {
        this.setState({
            isShow2: false
        });
        let data = [];
        if (e == 1) {
            data = this.state.oldLists.filter(item => item.eventType == true);
        } else if (e == 2) {
            data = this.state.oldLists.filter(item => item.eventType == false);
        } else if (e == 3) {
            data = this.state.oldLists;
        }

        this.setState({
            lists: data
        });

    }
    next = () => {

    }

    render() {
        const state = this.state;


        return (
            <div className='home'>
                <div className='bg'>
                    {
                        state.timeSlot == 0 ?
                            <img className='banner' src={bg1}></img> : <img className='banner' src={bg2}></img>
                    }
                    <div className='content'>
                        {state.userName},{state.bannerContent}
                        <div id='content'></div>
                    </div>

                </div>
                <div className='container'>
                    <div className='Statistics'>
                        <img src={state.levelBg}></img>
                        <div className='content'>
                            <div className='title'>个人绩效</div>
                            <div className='amount'>{state.ownerMerits}</div>
                            <div className='bottom'>
                                <div>总绩效 <span>{state.totalMerits}</span></div>
                                <div>工龄 <span>{state.seniority}</span></div>
                            </div>
                        </div>
                    </div>

                    <div className='records'>
                        <div className='title'>明细记录</div>
                        <div className='box'>
                            <div className='title'>
                                <div>日期
                                    <img src={timeImg} onClick={this.datePickerOpen}></img>
                                    <DatePicker onChange={this.onChange} open={state.datePickerOpen}
                                        locale={LocalFormat}
                                        showToday={false}
                                        format='YYYY/MM/DD'
                                        bordered={false}
                                        placement='topLeft' />
                                </div>
                                <div>事项
                                    {/* <img onClick={this.open1} src={downImg}></img> */}
                                    {/* <div className={state.isShow1 ? 'isShow same-item' : 'same-item'}>
                                        <div onClick={this.set1}>月度加分</div>
                                        <div onClick={this.set1} >违纪扣分</div>
                                    </div> */}
                                </div>
                                <div>分值
                                    <img onClick={this.open2} src={downImg}></img>
                                    <div className={state.isShow2 ? 'isShow same-item' : 'same-item'}>
                                        <div onClick={this.set2.bind(this, 1)}>增加</div>
                                        <div onClick={this.set2.bind(this, 2)} className='duce'>减少</div>
                                        <div onClick={this.set2.bind(this, 3)}>全部</div>
                                    </div>
                                </div>
                            </div>
                            <div className='lists' ref={e => { this.myRef = e; }} onScroll={this.handleScroll}>
                                {
                                    state.lists.map((item, key) => {
                                        return (
                                            <Row className='item' key={key}>
                                                <Col span={10}>{item.created_at}</Col>
                                                <Col span={10} className='explain'><Tooltip title={item.explain}>{item.explain}</Tooltip></Col>
                                                <Col span={4} className={item.eventType ? '' : 'red'}>
                                                    {item.eventType ? '+' : '-'}
                                                    {item.amount}</Col>
                                            </Row>
                                        );
                                    })
                                }
                            </div>
                            <div className='next'>
                                {/* <img onClick={this.loadMoreDataFn} src={require('@assets/home/next.png')}></img> */}
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
