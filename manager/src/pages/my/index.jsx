import { Component } from 'react';
import { Button, Input } from 'antd';
import { sendWeb3, sendContracts } from '@utils/index';
import { messageWay } from '@src/utils/publicUtils';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            inputValue: '',
            account: 0
        };
    }
    componentDidMount() {
        this.init();
    }
    init = async () => {
        if (window.ethereum) {
            await sendWeb3.eth.getAccounts((err, res) => {
                this.setState({
                    address: res[0]
                });
            });
        }
        let count = await sendContracts.Shareholders.methods.totalAngelInvestorCount().call();
        this.setState({
            count: count
        });

        let arr = ['a', 'b', 'c'];
        let index = Math.floor(Math.random() * arr.length);

        console.log(arr[index]); // c
    };
    setInputValue = e => {
        this.setState({
            inputValue: e.target.value
        });
    };
    setClear = () => {
        this.setState({
            inputValue: ''
        });
    };
    setChange = () => {
        this.approve();
    };
    incrementReward = () => {
        if (this.state.inputValue == '') return;
        sendContracts.Shareholders.methods
            .incrementReward(this.state.inputValue.toWei())
            .send({
                from: this.state.address
            })
            .on('receipt', res => {
                messageWay('设置成功', 'success');
                this.setClear();
                console.log(res);
            })
            .on('error', err => {
                console.log(err);
                messageWay('取消', 'error');
            });
    };
    approve = async () => {
        let allowance = await sendContracts.LAHToken.methods
            .allowance(this.state.address, sendContracts.Shareholders._address)
            .call();
        console.log(allowance.fromWei());
        if (Number( allowance.fromWei()) >Number(this.state.inputValue)) {
            this.incrementReward();
        } else {
            this.setState({
                loading: true
            });
            sendContracts.LAHToken.methods
                .approve(
                    sendContracts.Shareholders._address,
                    sendWeb3.utils.toWei('100000000000000000000')
                )
                .send({ from: this.state.address })
                .on('receipt', () => {
                    messageWay(global.t('tips.message10'), 'success');
                    this.incrementReward();
                })
                .on('error', () => {
                    messageWay(global.t('tips.message11'), 'error');
                    this.setState({
                        loading: false
                    });
                });
        }
    };
    render() {
        const state = this.state;
        return (
            <div className={global.isPC ? 'pc_my' : 'my'}>
                <div className="box">
                    <div className="title">获得天使投资人资格的数量: {state.count}</div>

                    <div className="item">
                        <span>天使分红矿池注入奖励数量：</span>
                        <Input onInput={this.setInputValue} value={state.inputValue}></Input>

                        <Button className="same-btn" onClick={this.setChange}>
                            设置
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}
